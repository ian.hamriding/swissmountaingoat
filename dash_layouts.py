from dash_app import app
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_cytoscape as cyto
#import dash_table
import random
import glob, os, yaml
import plotly.express as px
import pandas as pd
from PIL import Image # find out image size
import json

from dash_utilities import image_resize

logo_src = "https://1.bp.blogspot.com/-sPH58sqGUoY/XffHiqHQq5I/AAAAAAAAPX8/VgaiJ6gnq54qZDYNWGH8YAFsRaRqFo63wCLcBGAsYHQ/s1600/logo.png"


def create_header(quotes, language ='en', whocalled = 'Explore'):

    quote = random.choice(quotes)

    if whocalled == "Explore": 
        explore = "> Explore <"
        blog = "Blog"
        about = "About"
    elif whocalled == "Blog":
        explore = "Explore"
        blog = "> Blog <"
        about = "About"
    else: 
        explore = "Explore"
        blog = "Blog"
        about = "> About <"


    return html.Div(
        [html.Div( [html.Div( [html.Img(src=logo_src, className='one column'),
                               html.H3("The Mountain Goat"),
                               html.I(quote) ],
                            className='height columns'),
                    html.Div([html.Br(),
                             dcc.RadioItems( id = "language", 
                                             options=[{'label': 'English',  'value': 'en'},
                                                      {'label': 'Français', 'value': 'fr'}],
                                             value= 'en',
                                             labelStyle={'display': 'inline-block'}),
                             ], className='four columns'),
                    ], className='row'),
            html.Br(),
            html.Div([ html.Div([ dcc.Link(explore, href='/index/',  className="tab first")], className="one column "),
                       html.Div([ dcc.Link(blog,    href='/blog/',   className="tab")],       className="one column "),
                       html.Div([ dcc.Link(about,   href='/about/',  className="tab")],       className="one column ")
                    ], className="row "),        
            html.Div([ html.Hr()], className="twelve columns ")
        ],
        id="header",
        className='twelve columns ' )


def create_explore_layout(dropdownlists, elements, stylesheet, theMap, quotes, language):

    return html.Div(
    
    # header
    [   html.Div( create_header(quotes, language = language, whocalled="Explore") ),

    # filters
        html.Div([
                html.Div([ html.H5("Filters")], className="four columns "),
                html.Div([ html.H5("Stats")], className="four columns "),
                html.Div([ html.H5("Selection")], className="four columns "),
                 ], className="row "),
        html.Div(
            [
                html.Div(
                    [   html.P('Altitude:', className="control_label"),
                        dcc.RangeSlider(
                            id='altitude_slider',
                            min=0,
                            max=4810,
                            value=[0, 4810],
                            marks={0:    '',
                                   1000: '1000 m',
                                   2000: '2000 m',
                                   3000: '3000 m',
                                   4000: '4000 m',
                                   5000: ''},
                            className="dcc_control"),     
                        html.Br(),                        
                        html.P('Activity:', className="control_label"),
                        dcc.Checklist(id = "activities", options=[
                            {'label': 'Hiking', 'value': 'hike'},
                            {'label': 'Mountaineering', 'value': 'mountaineering'},
                            {'label': 'Rock climbing', 'value': 'climbing'},
                            {'label': 'V. ferrata', 'value': 'ferrata'},
                            {'label': 'Ski touring', 'value': 'ski_touring'}],
                            value=['hike', 'mountaineering', 'climbing', 'ski_touring', 'ferrata'],
                            labelStyle={'display': 'inline-block'}
                            ),
                        html.Br(),    
                        html.P('Years:', className="control_label"),
                        dcc.RangeSlider(
                            id='year_slider',
                            min=1960,
                            max=2017,
                            value=[1990, 2010],
                            className="dcc_control"),     
                        html.Br(),    
                        html.P('Difficulty:', className="control_label"),               
                        dcc.RangeSlider(id = "grades", min=0,
                                        max=4,
                                        step=None,
                                        marks={
                                            0: 'F',
                                            1: 'PD',
                                            2: 'AD',
                                            3: 'D',
                                            4: 'TD'},
                                        value=[0, 3]),
                    ],
                    className="pretty_container four columns"
                ),
                html.Div(
                    [
                        html.Div(
                            [
                                html.Div(
                                    [   html.P("Places"),
                                        html.H6( id="nplaces",className="info_text" )
                                    ],
                                    id="places", className="pretty_container"),
                                html.Div(
                                    [
                                        html.Div(
                                            [
                                                html.P("Routes"),
                                                html.H6(id="nroutes", className="info_text")
                                            ],
                                            id="routes", className="pretty_container" ),
                                        html.Div(
                                            [
                                                html.P("Climbs"),
                                                html.H6(
                                                    id="nclimbs",
                                                    className="info_text"
                                                )
                                            ],
                                            id="climb",
                                            className="pretty_container"
                                        ),
                                        html.Div(
                                            [
                                                html.P("Participants"),
                                                html.H6(
                                                    id="nparticipants",
                                                    className="info_text"
                                                )
                                            ],
                                            id="participants",
                                            className="pretty_container"
                                        ),
                                    ],
                                    id="tripleContainer",
                                )

                            ],
                            id="infoContainer",
                            className="row"
                        ),
                        html.Div(
                            [
                                dcc.Graph(
                                    id='count_graph',
                                )
                            ],
                            id="countGraphContainer",
                            className="pretty_container"
                        )
                    ],
                    id="rightCol",
                    className="four columns"), 
            html.Div( [
                    html.P('Place:', className="control_label"),
                    dcc.Dropdown(
                            id='dropdownlist_places',
                            options=dropdownlists["places"],
                            #multi=True,
                            #value=list(WELL_STATUSES.keys()),
                            className="dcc_control"),
                    html.P('Routes:', className="control_label"),                            
                    dcc.Dropdown(
                            id='dropdownlist_routes',
                            options=dropdownlists["routes"],
                            #multi=True,
                            #value=list(WELL_STATUSES.keys()),
                            className="dcc_control"),                    
                    html.P('Participants:', className="control_label"),
                    dcc.Dropdown(
                            id='dropdownlist_participants',
                            options=dropdownlists["participants"],
                            #multi=True,
                            #value=list(WELL_STATUSES.keys()),
                            className="dcc_control"),
                    html.P('Climbs:', className="control_label"),                            
                        dcc.Dropdown(
                            id='dropdownlist_climbs',
                            options=dropdownlists["climbs"],
                            #multi=True,
                            #value=list(WELL_STATUSES.keys()),
                            className="dcc_control"),                            

            ], className="pretty_container four columns"),            ],
            className="row"
        ),
        html.Div([
        html.Div([ html.H5("Map")], className="six columns "),
        html.Div([ html.H5("Relations")], className="six columns "),
            ], className="row "),
        html.Div(
            [
                html.Div(
                    [
                        dcc.Graph(id='map', figure=theMap)
                    ],
                    className='pretty_container six columns',
                ),
                html.Div(
                    [
                        cyto.Cytoscape(id='cytoGraph',
                                            layout={'name': 'circle'},
                                            style={'width': '100%', 'height': '800px'},
                                            elements=elements,
                                            stylesheet=stylesheet
                                            )
                    ],
                    className='pretty_container six columns',
                ),
            ],
            className='row'
        ),
    html.H6("Nicolas Dupuis, 2019. Powered with Python, Dash and Linked Data."),    
    html.A("Reach out", href="https://www.linkedin.com/in/nicolas-dupuis-3961b43/")
    ],
    id="mainContainer",
    style={
        "display": "flex",
        "flex-direction": "column"
    },
)


def create_blog_layout(graphDB, quotes, months, blogEntry=None, language = 'en', pathname ='assets/blog/', max_width = 960):

    # Build drop down list for years, look at the folders we have
    blog_years = {}
    for year in next(os.walk(pathname))[1]:
        blog_years[year] = {"label": year, "value": year}
    displayed_years = [blog_years[year] for year in blog_years]
    
    # User pasted a URL, let's analyse that...
    if blogEntry:
        # we expect BlogEntry to look like something like "2019/10/Mont_Blanc". 
        # Year, Month and Climb name must be existing sub-folders under /assets/blog.
        # If not: reset URL to /blog/ and show the latest entry.
        if not os.path.exists('assets/blog/' + blogEntry):
            print("[NOTE]: Wrong URL, reset to /blog/")
            blogEntry = None
        else: 
            year = blogEntry.split('/')[0]
            month = blogEntry.split('/')[1]
            climb = blogEntry.split('/')[2]

    # Coming from the 'blog' top link, let's pick a year
    if blogEntry == None:
        year = max(next(os.walk(pathname))[1])

    # Define the Months dropdownlist, looking at folders we have under the chosen year
    displayed_months = []
    for folder in next(os.walk(pathname + str(year)))[1]:
        climbs = len(next(os.walk(pathname + str(year) + "/" + folder))[1])
        displayed_months.append({"label": months[folder][language] + " (" + str(climbs) + ")", "value": folder })

    # Coming from the 'blog' top link, let's pick a month and a climb
    if blogEntry == None:
        month = displayed_months[-1]["value"]
        climb = next(os.walk(pathname + str(year) + '/' + str(month)))[1][-1]
        blogEntry = year + '/' + month + '/' + climb

    # Update the Details frame
    try: 
        filename = 'assets/blog/' + blogEntry + '/text.yaml'
        with open(filename) as file:
            BlogEntryInfo = yaml.load(file, Loader=yaml.FullLoader)
        title = BlogEntryInfo["title"]
        details = BlogEntryInfo["summary"][language]
    except: 
        title=details=""
        BlogEntryInfo = ""

    # let's update the profil picture
    try: 
        profil = app.get_asset_url('blog/' + blogEntry + '/_profil.jpg')
        profil_height = height_resize('assets/blog/' +  blogEntry + '/_profil.jpg', new_width = 250)
    except:
        profil = ""
        profil_height = 0

    # find coordinates for main place in that climb
    try: 
        # if passed directly in the Yaml
        if BlogEntryInfo["place"]: 
            Latitude = BlogEntryInfo["place"][0]
            Longitude = BlogEntryInfo["place"][1]
            waypoints = pd.DataFrame({'Latitude': [Latitude], 'Longitude': [Longitude]}, columns = ['Latitude','Longitude'])

        # We'll have to look into the triple store for that climb and get the places
        else: 
            pass
    except:
        waypoints = pd.DataFrame ({'Latitude': [], 'Longitude': []}, columns = ['Latitude','Longitude'])

    # create the small map
    smallMap = px.scatter_mapbox(waypoints, lat="Latitude", lon="Longitude", zoom=6, height=230)
    smallMap.update_layout(mapbox_style="open-street-map")
    smallMap.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    # Load pictures from this blog entry. This will go into a hidden div, for callbacks purpose
    entry_directory = 'assets/blog/' + blogEntry + '/'
    pictures = sorted(glob.glob(entry_directory + '*.jpg') + \
                      glob.glob(entry_directory + '*.JPG') + \
                      glob.glob(entry_directory + '*.png') + \
                      glob.glob(entry_directory + '*.PNG'))
    pictures = [pic.split('/')[-1] for pic in pictures]
    pictures = [pic for pic in pictures if pic[0] != '_']

    # Pick the first image, to be displayed.
    image = "blog/" + blogEntry + "/" + pictures[0]

    # Image size
    img_width, img_height = image_resize('assets/' + image, max_width)

    # so we can serve this local image 
    image = app.get_asset_url(image)

    # load the story for that first image, if there's a Yaml file
    try: 
        story = BlogEntryInfo[image.split('/')[-1]][language]
    except:
        story = ""

    return html.Div([
                html.Div( [ create_header(quotes, language = language, whocalled="Blog")]),
                html.Div( [ html.Div( [ html.H6("Years"), 
                                        dcc.Dropdown(id = 'blog_years', 
                                                     options = displayed_years,
                                                     value   = year),
                                        html.Br(),
                                        html.H6("Months"), 
                                        dcc.Dropdown(id = 'blog_months',
                                                     options= displayed_months,
                                                     value  = month),
                                        html.Br(),
                                        html.H6("Blog entries"),
                                        dcc.Markdown(id = "blog_entries", children = ""),
                                        html.Br()                                        
                                    ], className="pretty_container two columns")
                          ]),
                html.Div( [ html.Div(id='BlogEntry', style={'display': 'none'}, children=blogEntry), # hidden div to store all blog entry name
                            html.Div(id='BlogEntryInfo', style={'display': 'none'}, children=json.dumps(BlogEntryInfo)), # hidden div to store the Yaml file
                            dcc.Markdown(id = "story", children = story),
                            html.Hr(),
                            html.Div(id='pictures', style={'display': 'none'}, children=pictures), # hidden div to store all the image names
                            html.Img(id = 'image', src=image, width = img_width, height = img_height)
                            ], className = "pretty_container seven columns"),
                html.Div( [ html.Div( [
                                        html.Div([ html.Div([ html.Button("|<", id="first_picture")]),
                                                        html.Div([ html.Button("<", id="previous_picture")]),
                                                        html.Div([ html.Button(">", id="next_picture")]),
                                                        html.Div([ html.Button(">|", id="last_picture")])
                                                        ], className="row "),   
                                        html.Hr(),
                                        html.H4(id="climb_title", children = title),
                                                            html.H6(id="climb_details", children = details),
                                                            html.Hr(),
                                                            dcc.Graph(id='smallMap', figure=smallMap),
                                                            html.Hr(),
                                                            html.Img(id = 'climb_profil', src=profil, width = 250, height = profil_height),

                                                        ], className="pretty_container two columns")
                          ]),                        
                     ])
             
def create_about_layout(about, quotes, language ='en'):

      return html.Div([
                html.Div( [ create_header(quotes, language = language, whocalled="About")]),
                html.Div( [ html.Div( [ dcc.Markdown(id = 'about', children = about[language]),
                                        html.Br()                                        
                                    ], className="pretty_container three columns")
                          ]),               
                     ])