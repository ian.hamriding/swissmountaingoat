import dash

#------------------------------------------------------------------------------------
# Initialize the application and create the layout
#------------------------------------------------------------------------------------

app = dash.Dash(__name__, external_stylesheets=["assets//styles.css"])
app.title = "The Mountain Goat"
server = app.server

# suppress error message when loading callbacks with components not yet in the app's layout
app.config.suppress_callback_exceptions = True
