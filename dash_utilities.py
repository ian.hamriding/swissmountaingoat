import rdflib
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON
import yaml
import os
from PIL import Image # find out image size


# resize an image height, keeping original ration
def image_resize(img, max_width):

    # real image size & ratio
    width, height = Image.open(img).size
    ratio = width / height

    # new width: mininum of real width and max width
    new_width = min(width, max_width)

    # calculte new height
    new_height = new_width / ratio

    return (new_width, new_height)


def build_dropdownlists(graphDB, lists):
    ''' Perform a SPARQL and convert the result in a dictionnary ''' 

    dropdownlists = {}

    # for every request
    for key in lists:

        query = ''' PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX mountaingoat: <http://www.themountaingoat.org/uri/#>
                    SELECT ?s 
                    WHERE { ?s rdf:type ''' + lists[key] + ''' . }
                 '''

        # Perform the SPARQL query
        results = graphDB.query(query)

        items = []

        if results:  

            # let's sort it alphabetically
            results = sorted(list(results)) 

            # get a label from the node. To be improved!
            for row in results: 
                node = row[0].split('#')[-1]
                items.append({'label': node.split("#")[-1], 'value': row[0]})
        else:
            items = [{'label': '', 'value': ''}]

        dropdownlists[key] = items

    return dropdownlists



#------------------------------------------------------------------------------------
# Convert RDF data into Cytoscape component properties (elements and stylesheet)
#------------------------------------------------------------------------------------

def rdf_to_cytoscape(graphDB, node):
    ''' Convert RDF triples into a dict consumable by Cytoscape. Looking like this: 
    
            [{'data': {'id': 'A'}}, {'data': {'id': 'B'}},              # nodes definitions
             {'data': {'id': 'BA', 'source': 'B', 'target': 'A'}} ]     # relationships
     '''
    
    # Add Subject to the 'element' property. Using this page for reference https://dash.plot.ly/cytoscape/styling
    id_subject = "N1"
    short_s = graphDB.label(node, default = node.split("#")[-1])
    elements = [{'data': {'id': id_subject, 'label': short_s}, 'URI': node } ]
    stylesheet = [{'selector': 'node', 'style': {'label': 'data(label)', 'background-fit': 'cover'}},
                  {'selector': 'edge', 'style': {'curve-style': 'bezier'}} ]
 
    id = 1
    # Retrieve all triples about this Subject, and get their predicates and objects
    for s, p, o in graphDB.triples( (node, None , None)) :
        
        # I'm not interested to display ontological statements on the graph
        if ("owl" not in o) and ('type' not in p) and ('label' not in p):

            # get Object label. If none, use the last part of the URI, after the last slash
            short_o = graphDB.label(o, default = o.split("#")[-1])

            # add 'Object' node
            id += 1
            id_object = "N" + str(id)
            elements.append({'data': {'id': id_object, 'label': short_o, 'URI': o }})
            
            # add an edge, i.e the predicate between subject and object
            pred_id = id_object + id_subject
            elements.append(  {'data': {'id': pred_id, 'source': id_object, 'target': id_subject }})

            # add label for predicate
            short_p = graphDB.label(p, default = p.split("#")[-1])

            # Cytoscape stylesheet property
            stylesheet.append( {'selector': '#' + pred_id,
                               'style': {'source-arrow-color': 'blue',
                                         'source-arrow-shape': 'triangle',
                                         'line-color': 'blue',
                                         'label': short_p}} )

    return (elements, stylesheet)


#------------------------------------------------------------------------------------
# Convert RDF triples about places into a Pandas dataframe, to be put on the map
#------------------------------------------------------------------------------------
def create_waypoints_df(graphDB, user_filter=None):

    if user_filter:
        if user_filter['altitudes']:

            alt_min, alt_max = user_filter['altitudes']

            altitude_pattern = '''?s db:altitude ?alt .
                        FILTER (?alt >= ''' + str(alt_min) + ''' && ?alt <= ''' + str(alt_max) + ''') .'''
    else:
        altitude_pattern = "?s db:altitude ?alt ."

    # Bring all the 'places' and their coordinates
    query = ''' PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX mountaingoat: <http://www.themountaingoat.org/uri/#>
                PREFIX db: <http://dbpedia.org/page#>
                SELECT ?s ?label ?lat ?long
                WHERE { ?s rdf:type mountaingoat:Place .
                        ?s rdfs:label ?label .
                        ?s db:longitude ?long .
                        ?s db:latitude ?lat .
                        ''' + altitude_pattern + ''' } '''

    # Perform the SPARQL query
    results = graphDB.query(query)    

    URIs = []
    labels = []
    latitudes = []
    longitudes = []
    
    if results:  
        for row in results:
            URIs.append(str(row[0]))
            labels.append(str(row[1]))
            latitudes.append(float(row[2]))
            longitudes.append(float(row[3]))

    df = pd.DataFrame({'URI': URIs, 'Place':labels, 'Latitude':latitudes, 'Longitude':longitudes})

    return df


def query_endpoint(subject, predicate, prefix ="http://dbpedia.org/resource/", endpoint="http://dbpedia.org/sparql"):

    # Define the SPARQL endpoint for Dbpedia
    sparql = SPARQLWrapper(endpoint)

    # Define our SPARQL query
    sparql.setQuery('''SELECT ?object
                    WHERE { <''' + prefix + subject + '''>
                            <''' + predicate + '''>
                            ?object } ''')

    # We'll want to have the result in JSON
    sparql.setReturnFormat(JSON)

    # Perform the query and do the JSON conversion
    results = sparql.query().convert()

    # How to print the values from the JSON object
    try: 
        _object = results["results"]["bindings"][0]['object']["value"]
    except:
        _object = None
    
    return _object