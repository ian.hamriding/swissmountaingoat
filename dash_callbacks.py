import dash
from dash_app import app
from dash.dependencies import Input, Output, State
from dash_utilities import rdf_to_cytoscape, build_dropdownlists, create_waypoints_df, query_endpoint


#-------------------------------------------------------------------------------------------
# Data Entry page: search in Dbpedia for a new place, get coordinates and populate fields
#-------------------------------------------------------------------------------------------
@app.callback([Output('new_place_latitude', 'value'),
               Output('new_place_longitude','value'),
               Output('new_place_altitude', 'value'),
               Output('new_place_region',   'value')],
              [Input('check_dbpedia_for_place', 'n_clicks')],
              [State('new_place', 'value')])
def check_new_place(n_clicks, subject):

    predicates = {"latitude":  "http://www.w3.org/2003/01/geo/wgs84_pos#lat",
                  "longitude": "http://www.w3.org/2003/01/geo/wgs84_pos#long",
                  "altitude":  "http://dbpedia.org/ontology/elevation", 
                  "region":    "http://dbpedia.org/ontology/locatedInArea"}

    objects = []

    if n_clicks: 

        if n_clicks>0:

            for key in predicates:

                dbpedia_respond = query_endpoint(subject, predicates[key])
                
                if dbpedia_respond:
                    objects.append(dbpedia_respond)
                else:
                    objects.append("")

    return objects