# Layout for the data entry
def create_entry_layout(graphDB, dropdownlists, quotes):

    return html.Div(
[   html.Div( create_header(quotes) ),
        html.Div([ html.Br(),], className="twelve columns "),
        html.Div([ html.Div([ dcc.Link('Visualization     ',   href='/index/',   className="tab first")], className="one column "),
                   html.Div([ dcc.Link('Data Entry     '   ,   href='/entry/',   className="tab")],       className="two column "),
                 ], className="row "),        
        html.Div([ html.Hr()], className="twelve columns "),

        html.Div([

            # 1st bloc: new place
            html.Div([
                html.P("New place"), 
                html.Hr(),
                html.Div([ html.Div([html.H6("Name")],                  className = "three columns"),
                           html.Div([dcc.Input(type='text', id="new_place" )], className = "three columns")
                         ], className = "row"),
                html.Br(),
                html.Button("Dbpedia", id="check_dbpedia_for_place"),
                html.Br(),
                html.Br(),
                html.Div([ html.Div([html.H6("Latitude")],   className = "three columns"),
                           html.Div([dcc.Input(type='text', id= "new_place_latitude")], className = "three columns")
                         ], className = "row"),                         
                html.Div([ html.Div([html.H6("Longitude")], className = "three columns"),
                           html.Div([dcc.Input(type='text', id= "new_place_longitude")], className = "three columns")
                         ], className = "row"),     
                html.Div([ html.Div([html.H6("Altitude")],              className = "three columns"),
                           html.Div([dcc.Input(type='text', id= "new_place_altitude")], className = "three columns")
                         ], className = "row"),     
                html.Div([ html.Div([html.H6("Region")],              className = "three columns"),
                           html.Div([dcc.Input(type='text', id= "new_place_region")], className = "three columns")
                         ], className = "row"),     
                html.Br(),
                html.Br(),
                html.Button("Save",    id="save_place"),
                     ],  id="countainer1",  className="pretty_container three columns"),

            # 2nd bloc: New route
            html.Div([html.P("New route"), html.Hr()   ],  id="countainer2",  className="pretty_container three columns"),
            
            # 3rd bloc: New climb
            html.Div([
                html.P("New climb"),
                html.Hr(),
                html.Div([ html.Div([html.H6("Route")],   className = "three columns"),
                           html.Div([dcc.Input(type='text', id= "new_climb_route")], className = "three columns")
                         ], className = "row"),

                html.Div([ html.Div([html.H6("Participants")],   className = "three columns")]),
                dcc.Dropdown(
                            id='dropdownlist_add_participants',
                            options=dropdownlists["participants"],
                            #multi=True,
                            className="dcc_control"),
                html.Br(),
                html.Br(),
                html.Button("Save",    id="save_climb"),
                     ],  id="countainer3",  className="pretty_container three columns"),
            
            # 3rd bloc: New person
            html.Div([
                html.P("New participant"), 
                html.Hr(),
                html.Div([ html.Div([html.H6("Name")],                  className = "three columns"),
                           html.Div([dcc.Input(type='text', id="field_new_person" )], className = "three columns")
                         ], className = "row"),
                html.Br(),
                html.Button("Save",    id="save_person"),
                    ],  id="countainer4",  className="pretty_container three columns")


                ], className="row"),   



    html.H6("Nicolas Dupuis, 2019. Powered with Python, Dash and Linked Data."),    
    html.A("Reach out", href="https://www.linkedin.com/in/nicolas-dupuis-3961b43/")
    ],
    id="mainContainer",
    style={
        "display": "flex",
        "flex-direction": "column"
    },
)


#-------------------------------------------------------------------------------------------
# Save new participant
# ------------------------------------------------------------------------------------------

@app.callback([Output('field_new_person', 'value'),
               Output('dropdownlist_participants', 'options') ],
              [Input('save_person', 'n_clicks')],
              [State('field_new_person', 'value')])
def save_participant(n_clicks, participant):

    if n_clicks:
        if n_clicks > 0:

            # persist new triple in "triple store"
            rdf.add_triple_store("mountaingoat:" + participant, "rdf:" + "type", "mountaingoat:Participant") 

            # not safe in a multi user app
            print(len(rdf.graphDB))
            rdf.graphDB.add( (URIRef(rdf.namespace["mg"] + participant), RDF.type, URIRef(rdf.namespace["mg"] + "Participants")) ) 
            print(len(rdf.graphDB))

            #owl:NamedIndividual ,

        # update drop down list
        dropdownlist = build_dropdownlists(rdf.graphDB, {'participants': 'mountaingoat:Participant'}) 

        print(dropdownlist["participants"])

    return ("", dropdownlist)