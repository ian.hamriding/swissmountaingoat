'''
    Author: Nicolas Dupuis, 2019
'''

# Reference:
# https://github.com/plotly/dash-cytoscape
# https://github.com/plotly/dash-oil-and-gas-demo/blob/master/app.py

import pandas as pd
import glob, os
import yaml

import dash, flask
from dash_app import app
import dash_html_components as html
import dash_core_components as dcc
import dash_callbacks
from dash_layouts import create_header, create_explore_layout, create_blog_layout, create_about_layout
from dash.dependencies import Input, Output, State
from dash_utilities import rdf_to_cytoscape, build_dropdownlists, create_waypoints_df, query_endpoint, image_resize
import plotly.express as px

import json
from PIL import Image # find out image size

from rdf_management import RDF_management
from rdflib import URIRef, Literal, RDF, Namespace

quotes = ["At the top of the mountain, there’s another mountain.", 
          "The best view comes after the hardest climb.",
          "You are not in the mountains. The mountains are in you.",
          "All good things are wild and free.",
          "It’s not the mountain we conquer, but ourselves."]

months = {  '1': {'en': 'January', 'fr': 'Janvier'},
            '2': {'en': 'February', 'fr': 'Février'},
            '3': {'en': 'March', 'fr': 'Mars'},
            '4': {'en': 'April', 'fr': 'Avril'},
            '5': {'en': 'May', 'fr': 'Mai'},
            '6': {'en': 'June', 'fr': 'Juin'},
            '7': {'en': 'July', 'fr': 'Juillet'},
            '8': {'en': 'August', 'fr': 'Août'},
            '9': {'en': 'September', 'fr': 'Septembre'},
            '10': {'en': 'October', 'fr': 'Octobre'},
            '11': {'en': 'November', 'fr': 'Novembre'},
            '12': {'en': 'December', 'fr': 'Décembre'}
            }

about= {}
about['fr'] = "Bonjour visiteur,\n\nCeci est mon **nouveau** site web. Je l'ai construit (je veux dire programmé) moi même en utilisant Python, Dash et les Linked Data. Plus à venir. \n\n Nicolas"
about['en'] = "Hello visitor,\n\nThis is my **draft** new website about mountains. I built (i.e. programmed) myself using Python, Dash and Linked data. More to come. \n\n Nicolas"



# default language
languages = ["en","fr"]
language = languages[0]

max_width = 960

# create the App
app = dash.Dash(__name__, external_stylesheets=["assets//styles.css"])
app.title = "The Mountain Goat"
server = app.server
app.config.suppress_callback_exceptions = True


# Data management
# ----------------------------------------------------------------------------

# First, load RDF data into a rdf object
rdf = RDF_management("data", "ontology.owl")

# Infer triples using OWL constructs
rdf.graphDB = rdf.infer_triples("owl:SymmetricProperty")

# Dash expects dropdownlist items to be in dictionnaries. We'll create them: run SPARQL queries to fetch data and then convert to dict.
# This function returns a dict of dict, to be consummed in the Dash layout.
dropdownlists = build_dropdownlists(rdf.graphDB, {'climbs':       'mountaingoat:Climb',
                                                  'routes':       'mountaingoat:Route',
                                                  'places':       'mountaingoat:Place',
                                                  'participants': 'mountaingoat:Participant'}) 

# The Dash Cytoscape components expects 2 dictionnaries: 'elements' and 'stylesheet'.
# They contain the graph nodes and how to display them. Let's create them with a default node.
node = '''http://www.themountaingoat.org/uri/#Sommêtres_2019_01_01'''
elements, stylesheet = rdf_to_cytoscape(rdf.graphDB, URIRef(node))


# Define the map
# ----------------------------------------------------------------------------

# Dash map component needs a dataframe to tag places. Let's create it from the triples
waypoints = create_waypoints_df(rdf.graphDB)

theMap = px.scatter_mapbox(waypoints, lat="Latitude", lon="Longitude", hover_name="Place", hover_data=["URI", "Place"], color_discrete_sequence=["blue"], zoom=8, height=1000)
theMap.update_layout(mapbox_style="open-street-map")
theMap.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

# Layouts
# ----------------------------------------------------------------------------
app.layout = html.Div([dcc.Location(id='url', refresh=False), html.Div(id='page-content')])

#-------------------------------------------------------------------------------------------
# Update the layout when changing page
#-------------------------------------------------------------------------------------------
@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    
    language = "en"

    if pathname: 
        if "/blog/" in pathname: 

            if pathname.split("/blog/")[-1] != "":
                blogEntry = pathname.split("/blog/")[-1]
            else:
                blogEntry = None

            return create_blog_layout(rdf.graphDB, quotes, months, blogEntry, language)

        elif "/about/" in pathname:

            return create_about_layout(about, quotes, language)

        else:
            return create_explore_layout(dropdownlists, elements, stylesheet, theMap, quotes, language)         

#-------------------------------------------------------------------------------------------
# Update the blog_months dropdownlist when new year is picked
#-------------------------------------------------------------------------------------------
@app.callback([Output('blog_months', 'options'),
               Output('blog_months', 'value')],
              [Input('blog_years', 'value'),
               Input('language', 'value')], 
              [State('url', 'pathname')])
def update_blog_months(year, language, url):

    if year:
        blog_months = []
        for folder in next(os.walk('assets/blog/' + str(year)))[1]:
            climbs = len(next(os.walk('assets/blog/' + str(year) + "/" + folder))[1])
            blog_months.append({"label": months[folder][language] + " (" + str(climbs) + ")", "value": folder })
        return (blog_months, blog_months[-1]["value"])
    else:
        return ('','')

#-------------------------------------------------------------------------------------------
# Update the list of blog entries a when new month is picked
#-------------------------------------------------------------------------------------------
@app.callback(Output('blog_entries', 'children'),               
              [Input('blog_months', 'value')], 
              [State('blog_years', 'value'), 
               State('url', 'href')])
def update_blog_entries(month, year, url):

    print(url)

    # only when really called
    if month:
        markdown = ""
        for folder in next(os.walk('assets/blog/' + year + "/" + month ))[1]:
            markdown += '[' + folder + '](' + url + year + '/' + month + '/' + folder + ') \n'
        return markdown
    else:
        return ''


#-------------------------------------------------------------------------------------------
# Update the blog entry summary when language is changed
#-------------------------------------------------------------------------------------------
@app.callback(Output('climb_details', 'children'),
              [Input('language', 'value')], 
              [State('BlogEntryInfo', 'children')])
def update_blog_summary(language, BlogEntryInfo):

    if language:
        BlogEntryInfo = json.loads(BlogEntryInfo)
        return BlogEntryInfo["summary"][language]
    else:
        return ''

#-------------------------------------------------------------------------------------------
# Update the about me when language is changed
#-------------------------------------------------------------------------------------------
@app.callback(Output('about', 'children'),
              [Input('language', 'value')]) 
def update_about(language):

    if language:
        return about[language]
    else:
        return ''

#-------------------------------------------------------------------------------------------
# Update the blog picture & story when pressing previous/next buttons
#-------------------------------------------------------------------------------------------
@app.callback([Output('image', 'src'),
               Output('image', 'width'),
               Output('image', 'height'),
               Output('story', 'children')],
              [Input('previous_picture', 'n_clicks'), 
               Input('next_picture', 'n_clicks'),
               Input('first_picture', 'n_clicks'), 
               Input('last_picture', 'n_clicks'),
               Input('language', 'value')],
              [State('pictures', 'children'),
               State('image', 'src'),
               State('BlogEntry', 'children'),
               State('BlogEntryInfo', 'children')])
def update_image_src(previous, next, first, last, language, pictures, current_image, BlogEntry, BlogEntryInfo):

    context = dash.callback_context

    if context.triggered:

        i = pictures.index(current_image.split("/")[-1]) # Which image is the user currently viewing ?

        # user clicked on first button
        if context.triggered[0]["prop_id"] == "first_picture.n_clicks": 
            i =0

        # user clicked on last button
        if context.triggered[0]["prop_id"] == "last_picture.n_clicks": 
            i = len(pictures)-1

        # user clicked on next button
        if context.triggered[0]["prop_id"] == "next_picture.n_clicks" and i < len(pictures) -1: 
            i += 1            

        # user clicked on previous button                
        if context.triggered[0]["prop_id"] == "previous_picture.n_clicks" and i > 0: 
            i -= 1

        # add the proper path so it can be served as local file
        image = "blog/" + BlogEntry + '/' + pictures[i]
        
        # image size
        img_width, img_height = image_resize('assets/' + image, max_width)
    
        # convert the JSON string from the hidden dic into a dict.
        BlogEntryInfo = json.loads(BlogEntryInfo)

        # add the story
        try: 
            story = BlogEntryInfo[image.split('/')[-1]][language]
        except:
            story = ""
  
    else: 
        image = ""
        story = ""
        img_width = 0
        img_height = 0

    # asset_url allows to serve local files located under /assets
    return (app.get_asset_url(image), img_width, img_height, story)

# --------------------------------------------------------------------------------------------
# Update the Cytoscape graph
# --------------------------------------------------------------------------------------------
@app.callback(
    [Output('cytoGraph', 'elements'), 
     Output('cytoGraph', 'stylesheet')],
    [Input('dropdownlist_climbs', 'value'),
     Input('dropdownlist_routes', 'value'),
     Input('dropdownlist_places', 'value'),
     Input('dropdownlist_participants', 'value'),
     Input('cytoGraph', 'tapNodeData'),
     Input('map', 'hoverData') ])
def update_graph(climbs, routes, places, participants, selected_node, hoverData):
  
    # Dash tells us which input triggered the callback.
    context = dash.callback_context

    if context.triggered:

        # context.triggered is a dict with the guilty component name and its new property value
        component = context.triggered[0]["prop_id"]
        
        if component == "map.hoverData":
            node = context.triggered[0]["value"]['points'][0]['customdata'][0]
        elif component == "cytoGraph.tapNodeData":
            node = context.triggered[0]["value"]["URI"]
        else:
            node = context.triggered[0]["value"]

        elements, stylesheet = rdf_to_cytoscape(rdf.graphDB, URIRef(node))
    
    return (elements, stylesheet) # that's what Dash Outputs components get


#-------------------------------------------------------------------------------------------
# Update Map when filters are updated
#-------------------------------------------------------------------------------------------

@app.callback(Output(component_id='map', component_property='figure'),
              [Input('altitude_slider', 'value'), 
               Input('activities', 'value'), 
               Input('year_slider', 'value'), 
               Input('grades', 'value'), 
              ])
def update_theMap(altitude_range, activities, years, grades):

    user_filter = {}

    user_filter["altitudes"] = altitude_range
    user_filter["activities"] = activities
    user_filter["years"] = years
    user_filter["grades"] = grades

    # convert triples to dataframe, to feed theMap
    filtered_waypoints = create_waypoints_df(rdf.graphDB, user_filter)

    # recreate the map, with filtered waypoints
    theMap = px.scatter_mapbox(filtered_waypoints, lat="Latitude", lon="Longitude", hover_name="Place", hover_data=["URI", "Place"], color_discrete_sequence=["blue"], zoom=8, height=1000)
    theMap.update_layout(mapbox_style="open-street-map")
    theMap.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})

    return theMap


#------------------------------------------------------------------------------------
# Here we go!
#------------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run_server(debug=True) 

