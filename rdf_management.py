import rdflib
from rdflib import Graph, URIRef, BNode, Literal, Namespace, ConjunctiveGraph
from rdflib.serializer import Serializer
from rdflib.store import NO_STORE, VALID_STORE

class RDF_management:
    ''' Load RDF data and provide methods like inference '''

    def __init__ (self, DataFolder, RDFfile, RDF_serialization = 'turtle'):

        self.DataFolder = DataFolder
        self.RDFfile = RDFfile
        self.RDF_serialization = RDF_serialization

        # Define SPARQL queries for OWL constructs
        self.inferences = {}

        # 1) Infer triples using predicates defined as symetric. Better read my SPARQL query :-)
        self.inferences["owl:SymmetricProperty"]  = ''' PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                                                        PREFIX owl: <http://www.w3.org/2002/07/owl#>
                                                        CONSTRUCT { ?o ?symprop ?s . }
                                                        WHERE { ?symprop rdf:type owl:SymmetricProperty .
                                                                ?s ?symprop ?o  . }
                                                    '''

        # load the triples from the RDF file, using RDFLib Parse method
        self.graphDB = Graph().parse(DataFolder +"//" + RDFfile, format=RDF_serialization)

        self.namespace = {  ":":    "http://www.themountaingoat.org/uri/#", 
                            "mg":   "http://www.themountaingoat.org/uri/#", 
                            "owl":  "http://www.w3.org/2002/07/owl#",
                            "rdf":  "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                            "xml":  "http://www.w3.org/XML/1998/namespace", 
                            "xsd":  "http://www.w3.org/2001/XMLSchema#", 
                            "page": "http://dbpedia.org/page#", 
                            "rdfs": "http://www.w3.org/2000/01/rdf-schema#"}

        # Print the number of triples
        print("[Note] This graph has %s statements." % len(self.graphDB))

        # Print triples
        #print(self.graphDB.serialize(format='turtle'))

    def infer_triples(self, OWLconstruct):
        ''' Perform a query to infer new triples and merge them to the graph.
            Until I plug a semantic reasoner... '''
        
        # perform SPARQL query for the given OWL construct
        new_triples = self.graphDB.query(self.inferences[OWLconstruct]) 
        
        # add these new triples to the main graph
        self.graphDB += new_triples

        print("[Note] I have infered {} triples due to {}. ".format(len(new_triples), OWLconstruct))

        return self.graphDB


    # Append a new triple in the "triple store", using RDF Turtle serialization
    def add_triple_store(self, s, p, o, file="data//ontology.owl"):

        triple = s + " " + p + " " + o + " ."

        with open(file, "a") as myfile:
            myfile.write("\n# added by the App \n")
            myfile.write(triple + "\n\n")