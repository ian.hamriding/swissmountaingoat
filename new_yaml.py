# generate a new yaml file

import glob

entry = 'assets/blog/' + "2019/2019_11_24_Hundsrügg" + '/'

        
images = sorted(glob.glob(entry + '*.jpg') + \
                glob.glob(entry + '*.JPG') + \
                glob.glob(entry + '*.png') + \
                glob.glob(entry + '*.PNG'))

# don't keep images that start with an underscore
images = [img.split('/')[-1] for img in images if img.split('/')[-1][0] != '_']

yaml = ''' # Yaml definition for *''' + entry + '''*
URI: ""
label: ""
title: ""
date: ""
type: ""
place: ""

summary:
    en: ""
    fr: ""\n
'''

for image in images:
    yaml += '''"''' + image + '''": \n''' 
    yaml += '''    en: " " \n''' 
    yaml += '''    fr: " " \n\n''' 

file = open(entry + "text.yaml","w") 
file.write(yaml)  
file.close() 
print("Done!")